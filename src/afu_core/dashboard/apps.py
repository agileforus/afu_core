from django.apps import AppConfig


class DashboardConfig(AppConfig):
    name = 'afu_core.dashboard'
