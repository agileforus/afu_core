from django.contrib import admin

class CustomAdminSite(admin.AdminSite):

    def get_app_list(self, request):
        app_list = super(CustomAdminSite, self).get_app_list(request)
        app_list.append({
            'name': 'Django RQ',
            'app_label': 'django-rq',
            'app_url': '/django-rq',
            'models': [{
                'name': 'RQ Queues',
                'object_name' : 'rq-queues',
                'perms': {},
                'admin_url': '/django-rq',
                'add_url': '',
                'view_only': True
            }]
        })
        return app_list