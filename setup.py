
from setuptools import setup, find_packages

about = {}
with open("src/afu_core/__about__.py") as fp:
    exec(fp.read(), about)


def readme():
    with open('README.md') as f:
        return f.read()


setup(name=about['__title__'],
      version=about['__version__'],
      description=readme(),
      url='',
      author=about['__author__'],
      author_email=about['__email__'],
      license=about['__license__'],
      packages=find_packages('src'),
      package_dir={'': 'src'},
      platforms=[],
      python_requires='>=3.7',
      install_requires=[
          "Django~=3.0",
          "rq-scheduler~=0.9",
          "django-rq~=2.3",
          "requests~=2.23",
          "python-dateutil~=2.8",
          "Jinja2~=2.11",
          "djangorestframework~=3.11",
          "django-webpack-loader~=0.7"
      ],
      setup_requires=['pytest-runner'],
      tests_require=['pytest', 'pylint', 'pylint-django'],
      classifiers=[
            # Documents for packaging python softwares
            # https://packaging.python.org/tutorials/distributing-packages/
            # https://pypi.python.org/pypi?%3Aaction=list_classifiers

            # How mature is this project? Common values are
            #   3 - Alpha
            #   4 - Beta
            #   5 - Production/Stable
            'Development Status :: 3 - Alpha',
            'Environment :: Web Environment',
            'Framework :: Django :: 3.0',

            # Indicate who your project is intended for
            'Intended Audience :: Developers',
            'Topic :: Software Development',

            # Pick your license as you wish (should match "license" above)
            'License :: OSI Approved :: MIT License',

            # Specify the Python versions you support here. In particular, ensure
            # that you indicate whether you support Python 2, Python 3 or both.
            'Programming Language :: Python :: 3.7 :: Only',
            'Programming Language :: Python :: 3.7',

            'Operating System :: POSIX :: Linux'
        ],
      entry_points={
      },
      scripts=['src/manage.py'],
      include_package_data=True,
      zip_safe=False)
