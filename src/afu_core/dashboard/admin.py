from django.contrib import admin
from .models import Job, Story
from .jobs import import_stories

import django_rq

# Register your models here.

def run_job(modeladmin, request, queryset):
    for job in queryset:
        django_rq.enqueue(import_stories, job.params)
    pass
run_job.short_description = "Execute job with params"

class JobAdmin(admin.ModelAdmin):
    fields = ['params']
    list_display = ('params',)
    actions = [run_job]

admin.site.register(Job, JobAdmin)

class StoryAdmin(admin.ModelAdmin):
    fields = ['key', 'title', 'category', 'status', 'team']
    list_display = ('key', 'title', 'category', 'status', 'team',)

admin.site.register(Story, StoryAdmin)

