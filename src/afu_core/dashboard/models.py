from django.db import models

# Create your models here.

class Job(models.Model):
    params = models.CharField(max_length=512)

class Story(models.Model):
    title = models.CharField(max_length=512)
    key = models.CharField(max_length=64)
    
    category = models.CharField(max_length=64, null=True)
    team = models.CharField(max_length=128, null=True)
    status = models.CharField(max_length=128, null=True)
    rank = models.CharField(max_length=64, null=True)

    epic = models.CharField(max_length=128, null=True)
    
    sprint = models.CharField(max_length=256, null=True)
    sprint_start_date = models.DateTimeField('sprint start date', null=True)
    sprint_end_date = models.DateTimeField('sprint end date', null=True)

    create_date = models.DateTimeField('create date', null=True)
    due_date = models.DateTimeField('due date', null=True)
    resolution_date = models.DateTimeField('resolution date', null=True)