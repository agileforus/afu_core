from django.http import HttpResponse
from rest_framework import viewsets
from rest_framework.response import Response
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

from .models import Story

import statistics

class StoryViewSet(viewsets.ReadOnlyModelViewSet):

    def done_by_month(self, request):
        stories = self.get_total_done_by_month()
        return Response(stories)
    
    def control_chart(self, request):
        stories = self.get_control_chart_last_year()
        return Response(stories)

    def get_total_done_by_month(self):
        """Total of stories that is done by month
        
        Returns:
            dict -- struct with total all stories is done by month 
        """
        stories_done = {}
        for story in Story.objects.exclude(resolution_date=None).order_by('resolution_date'):
            d = story.resolution_date.strftime("%m/%Y")
            if d not in stories_done:
                stories_done[d] = 0
            stories_done[d] = stories_done[d] + 1
        return stories_done
    
    def get_control_chart_last_year(self):
        """Control chart infos about last year of stories"""

        last_month = datetime.now()
        last_month = last_month.replace(day=1)

        last_month = last_month - relativedelta(months=12)

        stories_done = {}
        for story in Story.objects.exclude(resolution_date=None).filter(resolution_date__gte=last_month).order_by('resolution_date'):
            d = story.resolution_date.strftime("%m/%Y")
            if d not in stories_done:
                stories_done[d] = 0
            stories_done[d] = stories_done[d] + 1
        
        context = {
            'labels':  '\', \''.join(stories_done.keys()),
            'values':  ', '.join(str(x) for x in stories_done.values()),
            'done': stories_done,
            'stories_done_values_median': ', '.join(str(statistics.median(stories_done.values())) for x in stories_done.values()),
            'median_max_best': ', '.join(str( statistics.median(stories_done.values()) + (statistics.stdev(stories_done.values())*1) ) for x in stories_done.values()),
            'median_min_best': ', '.join(str( statistics.median(stories_done.values()) - (statistics.stdev(stories_done.values())*1) ) for x in stories_done.values()),
            'median_max_good': ', '.join(str( statistics.median(stories_done.values()) + (statistics.stdev(stories_done.values())*2) ) for x in stories_done.values()),
            'median_min_good': ', '.join(str( statistics.median(stories_done.values()) - (statistics.stdev(stories_done.values())*2) ) for x in stories_done.values()),
            'median_max_regular': ', '.join(str( statistics.median(stories_done.values()) + (statistics.stdev(stories_done.values())*3) ) for x in stories_done.values()),
            'median_min_regular': ', '.join(str( statistics.median(stories_done.values()) - (statistics.stdev(stories_done.values())*3) ) for x in stories_done.values()),
        }
        
        return context
