
from afu_core.jira_server_client import JiraServerClient
from afu_core.dashboard.models import Story
from dateutil.relativedelta import relativedelta
from django.conf import settings

import django_rq
import datetime
import logging
logger = logging.getLogger(__name__)

def import_stories(project):
    logger.debug("Import stories")
    jira_server_client = JiraServerClient()

    start_date = datetime.datetime(2019, 1, 1, 0, 0, 0, 0)
    today = datetime.datetime.now()
    while start_date < today:
        logger.debug("Importe stories from : %s" % start_date.strftime("%Y/%m/%d"))
        end_date = start_date
        end_date = end_date + relativedelta(months=1)
        end_date = end_date - relativedelta(days=1)

        jql = "project = %s AND issuetype not in (epic, Sub-tarefa) AND created >= '%s' AND created <= '%s' AND (resolutiondate IS EMPTY OR resolutiondate >= '2019/01/01') ORDER BY Rank ASC" % (project, start_date.strftime("%Y/%m/%d"), end_date.strftime("%Y/%m/%d"))
        
        data = {}
        issues = jira_server_client.search(jql)
        for issue in issues['issues']:
            item = get_item(issue, project)

            story = None
            try:
                story = Story.objects.get(key=item['key'])
            except Story.DoesNotExist:
                story = Story()

            story.title = item['summary']
            story.key = item['key']
            story.category = item['type']
            story.team = item['team']
            story.status = item['status']
            story.rank = item['rank']
            story.epic = item['epic']
            story.due_date = item['duedate']
            story.resolution_date = item['resolutiondate']
            story.save()
            
            # save the start date of sprint or todo when the resolution date is not None
            if item['resolutiondate'] is not None:
                django_rq.enqueue(update_sprint_start_date, project, item['key'])
            
        
        start_date = start_date + relativedelta(months=1)
        end_date = end_date + relativedelta(months=1)

def update_sprint_start_date(project, issue_key):
    logger.debug("Update Sprint Start Date")
    jira_server_client = JiraServerClient()
    try:
        issue = jira_server_client.changelog(issue_key)
        story = Story.objects.get(key=issue_key)
        sprints = issue['fields'][settings.JIRA_SERVER['field_sprint']]
        sprint_start_date = jira_server_client.get_sprint_start_date(sprints)
        if sprint_start_date is not None:
            story.sprint_start_date = sprint_start_date
        else:
            story.sprint_start_date = jira_server_client.get_todo_start_date(issue)

    except Story.DoesNotExist:
        logger.error("Not exist the story: %s" % issue_key)

def get_item(issue, project):
    resolutiondate = datetime.datetime.strptime(issue['fields']['resolutiondate'], "%Y-%m-%dT%H:%M:%S.%f%z") if issue['fields']['resolutiondate'] != None else None
    month_resolutiondate = datetime.datetime.strptime(issue['fields']['resolutiondate'], "%Y-%m-%dT%H:%M:%S.%f%z") if issue['fields']['resolutiondate'] != None else None
    if month_resolutiondate is not None:
        month_resolutiondate = month_resolutiondate.replace(day=1, hour=0, minute=0, second=0)
    return {
                "key": issue['key'],
                "type": issue['fields']['issuetype']['name'],
                "summary": issue['fields']['summary'],
                "team": project if issue['fields']['customfield_14926'] is None else issue['fields']['customfield_14926']['name'],
                "duedate": issue['fields']['duedate'],
                "resolutiondate": resolutiondate if resolutiondate != None else None,
                "status": issue['fields']['status']['name'],
                "epic": issue['fields']['customfield_12602'],
                "rank": issue['fields']['customfield_12605'],
                "sprint": issue['fields']['customfield_12600'],
                "month_resolutiondate": month_resolutiondate if month_resolutiondate != None else None,
    }