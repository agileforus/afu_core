"""afu_core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.views.generic import TemplateView
from django.urls import path, include

from .dashboard import views
from .dashboard.story_view_set import StoryViewSet

done_by_month = StoryViewSet.as_view({'get': 'done_by_month'})
stories_control_chart = StoryViewSet.as_view({'get': 'control_chart'})

urlpatterns = [
    path('admin/', admin.site.urls),
    path('django-rq/', include('django_rq.urls')),
    path('', TemplateView.as_view(template_name='webapp/index.html')),
    path('control-chart/', views.control_chart, name='control_chart'),
    path('stories/done_by_month/', done_by_month),
    path('stories/control-chart/', stories_control_chart),
]
