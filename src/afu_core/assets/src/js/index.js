import React from 'react';
import ReactDOM from 'react-dom';
import Button from '@material-ui/core/Button';

import '../scss/index.scss';
import './Dashboard.js'
import Dashboard from './Dashboard.js';

class App extends React.Component {
  render () {
    return (
        <Dashboard></Dashboard>
    )
  }
}
ReactDOM.render(<App />, document.getElementById('react-app'));