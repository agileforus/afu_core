.PHONY: help run

VENV_NAME?=.venv
VENV_ACTIVATE=. $(VENV_NAME)/bin/activate
PYTHON=${VENV_NAME}/bin/python3

.DEFAULT: help

help:
	@echo "make run"
	@echo "       run project"

server:
	manage.py runserver

webapp:
	cd src && npm run watch

worker:
	manage.py rqworker default
	
scheduler:
	manage.py rqscheduler

