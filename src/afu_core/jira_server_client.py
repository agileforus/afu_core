
from dateutil.relativedelta import relativedelta
from django.conf import settings

import datetime
import requests
import urllib3
import os
import logging
import re

logger = logging.getLogger(__name__)
urllib3.disable_warnings()

class JiraServerClient(object):

    def __init__(self):
        self.username = settings.JIRA_SERVER['username']
        self.password = settings.JIRA_SERVER['password']
        self.host = settings.JIRA_SERVER['host']
        self.issue_endpoint = settings.JIRA_SERVER['issue_endpoint']
        self.search_endpoint = settings.JIRA_SERVER['search_endpoint']
        self.fields = settings.JIRA_SERVER['fields']

        self.proxies = None        
        if settings.PROXY['use_proxy']:
            logger.error("use proxy")
            self.proxies = {
                'http': settings.PROXY['http'],
                'https': settings.PROXY['https'],
            }

    def search(self, jql):
        logger.debug("Search JQL: %s" % jql)
        endpoint = "%s/%s?jql=%s&fields=%s&maxResults=1000" % (self.host, self.search_endpoint, jql, self.fields)
        r = requests.get(
            endpoint,
            auth=(self.username, self.password),
            headers={
                "Content-Type": "application/json"
            },
            proxies=self.proxies,
            verify=False
        )
        logger.debug("Status code %s" % r.status_code)
        if r.status_code == 200:
            logger.debug(r.json())
            return r.json()
        raise Exception("Not find search on Jira. \nendpoint: %s \nstatus_code: %s \nresponde: %s" % (endpoint, r.status_code, r.text))


    def changelog(self, key):
        endpoint = "%s/%s/%s?fields=%s&expand=changelog" % (self.host, self.issue_endpoint, key, self.fields)
        r = requests.get(
            endpoint,
            auth=(self.username, self.password),
            headers={
                "Content-Type": "application/json"
            },
            proxies=self.proxies,
            verify=False
        )
        if r.status_code == 200:
            return r.json()
        raise Exception('Not possible search on JIRA. \nendpoint: %s \nstatus_code: %s' % (endpoint, r.status_code))


    def get_sprint_start_date(self, sprints):
        """From custom field sprint finds the first start date from regex in text for get
            the start date sprint
        
        Arguments:
            sprints {array} -- Array with sprints in text format
        
        Returns:
            datetime -- Date and time when firt sprint was started
        """        
        if sprints is None:
            return None
        sprint_start_date = None
        for sprint in sprints:
            infos = re.search(r".*startDate=(.*?),.*endDate=(.*?),.*", sprint)
            start_date = datetime.datetime.strptime(infos.group(1), "%Y-%m-%dT%H:%M:%S.%f%z")
            if sprint_start_date is None or start_date < sprint_start_date:
                sprint_start_date = start_date
        return sprint_start_date


    def get_todo_start_date(self, issue):
        """From changelog struct finds the 'todo' status for get the start date, 
            if not find this so return None.
        
        Arguments:
            changelog {dict} -- Dict in format from JIRA
        
        Returns:
            datetime -- Date and time when the status change to 'TO DO'
        """        
        if issue is None or issue['changelog']['total'] == 0:
            return None
        todo_start_date = None
        for history in issue['changelog']['histories']:
            for item in history['items']:
                for code in settings.JIRA_SERVER['fields_status_todo']:
                    if item['to'] == code:
                        start_date = datetime.datetime.strptime(history['created'], "%Y-%m-%dT%H:%M:%S.%f%z")
                        if todo_start_date is None or todo_start_date < start_date:
                            todo_start_date = start_date
        return todo_start_date

