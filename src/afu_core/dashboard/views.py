from django.shortcuts import render
from django.conf import settings

from .models import Story

from afu_core.jira_server_client import JiraServerClient

import datetime
import statistics
import json
import re

# Create your views here.

def index(request):
    
    stories_done = {}
    for story in Story.objects.exclude(resolution_date=None).order_by('resolution_date'):
        d = story.resolution_date.strftime("%m/%Y")
        if d not in stories_done:
            stories_done[d] = 0
        stories_done[d] = stories_done[d] + 1
    
    stories_team_done = {}
    for story in Story.objects.exclude(resolution_date=None).order_by('resolution_date'):
        d = story.resolution_date.strftime("%m/%Y")
        if story.team in ['CLE-OCL-OneCloud', 'CLD-PROJECTS', 'CLD-PO', 'CLD-SAR']:
            continue
        if story.team not in stories_team_done:
            stories_team_done[story.team] = {}
        for dd in stories_done.keys():
            if dd not in stories_team_done[story.team]:
                stories_team_done[story.team][dd] = 0
        stories_team_done[story.team][d] = stories_team_done[story.team][d] + 1
    print(stories_team_done)
    context = {
        'stories_done_keys':  '\', \''.join(stories_done.keys()),
        'stories_done_values':  ', '.join(str(x) for x in stories_done.values()),
        'stories_done_values_median': ', '.join(str(statistics.median(stories_done.values())) for x in stories_done.values()),
        'stories_done_values_median_max': ', '.join(str( statistics.median(stories_done.values()) + (statistics.stdev(stories_done.values())*3) ) for x in stories_done.values()),
        'stories_done_values_median_min': ', '.join(str( statistics.median(stories_done.values()) - (statistics.stdev(stories_done.values())*3) ) for x in stories_done.values()),
        'stories_team_done_values': stories_team_done
    }
    
    return render(request, 'dashboard/index.html', context)

def control_chart(request):

    stories = get_total_stories_done_by_month()

    ivi = Story.objects.filter(team="IVI").exclude(resolution_date=None)

    context = {
        'teams': settings.AFU_TEAMS['groups'],
        'stories': {
            'points': stories,
            'CL': statistics.mean(stories.values()),
            'UCL': statistics.mean(stories.values()) + (1 * statistics.stdev(stories.values())),
            'LCL': statistics.mean(stories.values()) - (1 * statistics.stdev(stories.values())),
        },
        'changelog': ivi[1].sprint_start_date
    }
    return render(request, 'dashboard/control_chart.html', context)

def get_total_stories_done_by_month():
    stories_done = {}
    for story in Story.objects.exclude(resolution_date=None).order_by('resolution_date'):
        d = story.resolution_date.strftime("%m/%Y")
        if d not in stories_done:
            stories_done[d] = 0
        stories_done[d] = stories_done[d] + 1
    return stories_done


